swagger: '2.0'
info:
  description: Vertical Service Management Function Cataolgue  API
  version: '1.0'
  title: ANCHOR CSMF Catalogue Management API
  termsOfService: urn:tos
  contact: {}
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0
host: localhost:8099
basePath: /
tags:

  - name: VSB Catalogue API
    description: Vertical Service Blueprint Catalogue API
  - name: VSD Catalogue API
    description: Vertical Service Descriptor Catalogue API
 
paths:
 
  /csmf/catalogue/vsblueprint:
    get:
      tags:
        - VSB Catalogue API
      summary: Get ALL the Vertical Service Blueprints
      operationId: getAllVsBlueprintsUsingGET
      produces:
        - '*/*'
     
        
      responses:
        '200':
          description: List of all the Vertical Service Blueprints
          schema:
            type: array
            items:
              $ref: '#/definitions/VsBlueprintInfo'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    post:
      tags:
        - VSB Catalogue API
      summary: Onboard a new Vertical Service Blueprint
      operationId: createVsBlueprintUsingPOST
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
       
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/OnBoardVsBlueprintRequest'
      responses:
        '201':
          description: The ID of the created Vertical Service Blueprint
          schema:
            type: string
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /csmf/catalogue/vsblueprint/{vsbId}:
    get:
      tags:
        - VSB Catalogue API
      summary: Get a Vertical Service Blueprint with a given ID
      operationId: getVsBlueprintUsingGET
      produces:
        - '*/*'
      parameters:
        
      
        - name: vsbId
          in: path
          description: vsbId
          required: true
          type: string
      responses:
        '200':
          description: Vertical Service Blueprint with the given ID
          schema:
            $ref: '#/definitions/VsBlueprintInfo'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    delete:
      tags:
        - VSB Catalogue API
      summary: Delete a Vertical Service Blueprint with a given ID
      operationId: deleteVsBlueprintUsingDELETE
      produces:
        - '*/*'
      parameters:
       
        - name: vsbId
          in: path
          description: vsbId
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            type: object
        
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
      deprecated: false
  /csmf/catalogue/vsdescriptor:
    get:
      tags:
        - VSD Catalogue API
      summary: Query ALL the Vertical Service Descriptor
      operationId: getAllVsDescriptorsUsingGET
      produces:
        - '*/*'
     
      responses:
        '200':
          description: List of all the Vertical Service Descriptor of the user.
          schema:
            type: array
            items:
              $ref: '#/definitions/VsDescriptor'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    post:
      tags:
        - VSD Catalogue API
      summary: Onboard a new Vertical Service Descriptor
      operationId: createVsDescriptorUsingPOST
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/OnboardVsDescriptorRequest'
      responses:
        '201':
          description: The ID of the created Vertical Service Descriptor.
          schema:
            type: string
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /csmf/catalogue/vsdescriptor/{vsdId}:
    get:
      tags:
        - VSD Catalogue API
      summary: Query a Vertical Service Descriptor with a given ID
      operationId: getVsDescriptorUsingGET
      produces:
        - '*/*'
      parameters:
       
        - name: vsdId
          in: path
          description: vsdId
          required: true
          type: string
      responses:
        '200':
          description: Details of the Vertical Service Descriptor with the given ID
          schema:
            $ref: '#/definitions/VsDescriptor'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    delete:
      tags:
        - VSD Catalogue API
      summary: Delete a Vertical Service Descriptor with the given ID
      operationId: deleteVsDescriptorUsingDELETE
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
       
        - name: vsdId
          in: path
          description: vsdId
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            type: object
        
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
      deprecated: false

definitions:
  ApplicationMetric:
    type: object
    properties:
      interval:
        type: string
      metricCollectionType:
        type: string
        enum:
          - CUMULATIVE
          - DELTA
          - GAUGE
      metricId:
        type: string
      name:
        type: string
      topic:
        type: string
      unit:
        type: string
    title: ApplicationMetric
  
 
  OnBoardVsBlueprintRequest:
    type: object
    properties:
      translationRules:
        type: array
        items:
          $ref: '#/definitions/VsdNstTranslationRule'
      vsBlueprint:
        $ref: '#/definitions/VsBlueprint'
    title: OnBoardVsBlueprintRequest
  OnboardVsDescriptorRequest:
    type: object
    properties:
      nestedVsd:
        type: object
        additionalProperties:
          $ref: '#/definitions/VsDescriptor'
      tenantId:
        type: string
      vsd:
        $ref: '#/definitions/VsDescriptor'
    title: OnboardVsDescriptorRequest
 
  ServiceConstraints:
    type: object
    properties:
      atomicComponentId:
        type: string
      canIncludeSharedElements:
        type: boolean
      nonPreferredProviders:
        type: array
        items:
          type: string
      preferredProviders:
        type: array
        items:
          type: string
      priority:
        type: string
        enum:
          - LOW
          - MEDIUM
          - HIGH
      prohibitedProviders:
        type: array
        items:
          type: string
      sharable:
        type: boolean
    title: ServiceConstraints
 
 
  SliceServiceParameters:
    type: object
    discriminator: type
    title: SliceServiceParameters
 

 
  

  VsBlueprint:
    type: object
    properties:
      applicationMetrics:
        type: array
        items:
          $ref: '#/definitions/ApplicationMetric'
      atomicComponents:
        type: array
        items:
          $ref: '#/definitions/VsComponent'
      blueprintId:
        type: string
      configurableParameters:
        type: array
        items:
          type: string
      connectivityServices:
        type: array
        items:
          $ref: '#/definitions/VsbLink'
      description:
        type: string
      embbServiceCategory:
        type: string
        enum:
          - URBAN_MACRO
          - RURAL_MACRO
          - INDOOR_HOTSPOT
          - BROADBAND_ACCESS_IN_A_CROWD
          - DENSE_URBAN
          - BROADBAND_LIKE_SERVICES
          - HIGH_SPEED_TRAIN
          - HIGH_SPEED_VEHICLE
          - AIRPLANES_CONNECTIVITY
      endPoints:
        type: array
        items:
          $ref: '#/definitions/VsbEndpoint'
      interSite:
        type: boolean
      name:
        type: string
      parameters:
        type: array
        items:
          $ref: '#/definitions/VsBlueprintParameter'
      serviceSequence:
        type: array
        items:
          $ref: '#/definitions/VsbForwardingPathHop'
      sliceServiceType:
        type: string
        enum:
          - NONE
          - EMBB
          - URLLC
          - M_IOT
          - ENTERPRISE
          - NFV_IAAS
      urllcServiceCategory:
        type: string
        enum:
          - DISCRETE_AUTOMATION
          - PROCESS_AUTOMATION_REMOTE_CONTROL
          - PROCESS_AUTOMATION_MONITORING
          - ELECTRICITY_DISTRIBUTION_HIGH_VOLTAGE
          - ELECTRICITY_DISTRIBUTION_MEDIUM_VOLTAGE
          - INTELLIGENT_TRANSPORT_SYSTEMS_INFRASTRUCTURE_BACKHAUL
      version:
        type: string
    title: VsBlueprint
  VsBlueprintInfo:
    type: object
    properties:
      activeVsdId:
        type: array
        items:
          type: string
      name:
        type: string
      vsBlueprint:
        $ref: '#/definitions/VsBlueprint'
      vsBlueprintId:
        type: string
      vsBlueprintVersion:
        type: string
    title: VsBlueprintInfo
  VsBlueprintParameter:
    type: object
    properties:
      applicabilityField:
        type: string
      parameterDescription:
        type: string
      parameterId:
        type: string
      parameterName:
        type: string
      parameterType:
        type: string
    title: VsBlueprintParameter
  VsComponent:
    type: object
    properties:
      associatedVsbId:
        type: string
      compatibleSite:
        type: string
      componentId:
        type: string
      endPointsIds:
        type: array
        items:
          type: string
      imagesUrls:
        type: array
        items:
          type: string
      lifecycleOperations:
        type: object
        additionalProperties:
          type: string
      placement:
        type: string
        enum:
          - EDGE
          - CLOUD
      serversNumber:
        type: integer
        format: int32
      type:
        type: string
        enum:
          - SERVICE
          - FUNCTION
          - OTHER
    title: VsComponent
  VsDescriptor:
    type: object
    properties:
      associatedVsdId:
        type: string
      domainId:
        type: string
      managementType:
        type: string
        enum:
          - PROVIDER_MANAGED
          - TENANT_MANAGED
      name:
        type: string
      nestedVsdIds:
        type: object
        additionalProperties:
          type: string
      qosParameters:
        type: object
        additionalProperties:
          type: string
      serviceConstraints:
        type: array
        items:
          $ref: '#/definitions/ServiceConstraints'
      sla:
        $ref: '#/definitions/VsdSla'
      sliceServiceParameters:
        $ref: '#/definitions/SliceServiceParameters'
      version:
        type: string
      vsBlueprintId:
        type: string
      vsDescriptorId:
        type: string
    title: VsDescriptor
  VsbEndpoint:
    type: object
    properties:
      endPointId:
        type: string
      external:
        type: boolean
      management:
        type: boolean
      ranConnection:
        type: boolean
    title: VsbEndpoint
  VsbForwardingPathEndPoint:
    type: object
    properties:
      endPointId:
        type: string
      vsComponentId:
        type: string
    title: VsbForwardingPathEndPoint
  VsbForwardingPathHop:
    type: object
    properties:
      hopEndPoints:
        type: array
        items:
          $ref: '#/definitions/VsbForwardingPathEndPoint'
    title: VsbForwardingPathHop
  VsbLink:
    type: object
    properties:
      connectivityProperties:
        type: array
        items:
          type: string
      endPointIds:
        type: array
        items:
          type: string
      external:
        type: boolean
      name:
        type: string
    title: VsbLink
  VsdNstTranslationRule:
    type: object
    properties:
      blueprintId:
        type: string
      id:
        type: integer
        format: int64
      input:
        type: array
        items:
          $ref: '#/definitions/VsdParameterValueRange'
      nstId:
        type: string
    title: VsdNstTranslationRule
  VsdParameterValueRange:
    type: object
    properties:
      maxValue:
        type: integer
        format: int32
      minValue:
        type: integer
        format: int32
      parameterId:
        type: string
    title: VsdParameterValueRange
  VsdSla:
    type: object
    properties:
      availabilityCoverage:
        type: string
        enum:
          - AVAILABILITY_COVERAGE_HIGH
          - AVAILABILITY_COVERAGE_MEDIUM
          - UNDEFINED
      lowCostRequired:
        type: boolean
      serviceCreationTime:
        type: string
        enum:
          - SERVICE_CREATION_TIME_LOW
          - SERVICE_CREATION_TIME_MEDIUM
          - UNDEFINED
    title: VsdSla
 
 

